FROM rocker/rstudio

RUN  apt update && apt install s3fs -y

ENV \
    ROOT=TRUE \
    DISABLE_AUTH=TRUE \
